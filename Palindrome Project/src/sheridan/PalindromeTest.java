package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPalindrome( )  {
		assertTrue("Incorrect value provided " , Palindrome.isPalindrome( "never odd or even") );
	}
	
	
	@Test 
	public void testPalindromeNegative(  ) {
		assertFalse("Incorrect value provided " , Palindrome.isPalindrome( "this  is not a palindrome") );
	}
	
	@Test
	public void testPalindromeBoundaryIn( ) {
		assertTrue( "Incorrect value provided" , Palindrome.isPalindrome( "abcdcba")  );
	}

	@Test
	public void testPalindromeBoundaryOut( ) {
		assertFalse( "Incorrect value provided" , Palindrome.isPalindrome( "abcdfcba")  );
	}	
	
	
}
